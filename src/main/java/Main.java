import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Main {
    public static void main(String[] args) throws InterruptedException {

        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        //driver.manage().window().setSize(new Dimension(1920, 1080));
        driver.get("https://www.google.com.ua/?hl=ru");

        JavascriptExecutor jsq = (JavascriptExecutor)driver;
        jsq.executeScript("window.open()");
        List<String> wh = new ArrayList<>(driver.getWindowHandles());

        System.out.println(wh);

        driver.switchTo().window(wh.get(1));

        driver.navigate().to("https://www.youtube.com/");
        WebElement elm_search = driver.findElement(By.xpath("/html/body/ytd-app/div/div/ytd-masthead/div[3]/div[2]/ytd-searchbox"));
        elm_search.click();
        elm_search.sendKeys("MacBook");     //ищем Макбук
        elm_search.sendKeys(Keys.ENTER);

        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        WebElement elm_filter = driver.findElement(By.xpath("/html/body/ytd-app/div/ytd-page-manager/ytd-search/div[1]/ytd-two-column-search-results-renderer/div/ytd-section-list-renderer/div[1]/div[2]/ytd-search-sub-menu-renderer/div[1]/div/ytd-toggle-button-renderer/a"));
        elm_filter.click();       //нажимаем на фильтр


        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        WebElement elm_date   = driver.findElement(By.xpath("/html/body/ytd-app/div/ytd-page-manager/ytd-search/div[1]/ytd-two-column-search-results-renderer/div/ytd-section-list-renderer/div[1]/div[2]/ytd-search-sub-menu-renderer/div[1]/iron-collapse/div/ytd-search-filter-group-renderer[1]/ytd-search-filter-renderer[3]/a"));
        elm_date.click();         //за последнюю неделю



        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        List<WebElement> videos = driver.findElements(By.xpath("//h3"));    //заголовки (видео)
        System.out.println(videos);
        for (WebElement i: videos) {
            if(i.getText().contains("обзор")){
                i.click();
                break;
            }
        }


        try {
            TimeUnit.SECONDS.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

       WebElement elm_subscribe;

        while (true) {
            elm_subscribe = driver.findElement(By.xpath("/html/body/ytd-app/div/ytd-page-manager/ytd-watch-flexy/div[5]/div[1]/div/div[8]/div[3]/ytd-video-secondary-info-renderer/div/div[2]/div/ytd-button-renderer/a"));
            if (elm_subscribe.isDisplayed()) {
                break;
            } else {
                //Thread.sleep(100);
                TimeUnit.SECONDS.sleep(2);
            }
        }

        elm_subscribe.click();            //жмем подписку


//        driver.close();
//        System.out.println(driver);
//        driver.quit();
//        System.out.println(driver);

    }
    }
